package com.qudini.quicksetup.server.service;

import java.util.UUID;

public class Server {

    private UUID id;
    private String name;
    private boolean isAnon;
    private boolean isOnShift;
    private boolean isServing;

    public boolean isOnShift() {
        return isOnShift;
    }

    public void setOnShift(boolean onShift) {
        isOnShift = onShift;
    }

    public boolean isServing() {
        return isServing;
    }

    public void setServing(boolean serving) {
        isServing = serving;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAnon() {
        return isAnon;
    }

    public void setAnon(boolean anon) {
        isAnon = anon;
    }
}
