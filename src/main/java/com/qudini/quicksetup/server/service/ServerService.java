package com.qudini.quicksetup.server.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ServerService {

    private List<Server> serverList = new ArrayList<>();

    public ServerService() {
        addServer("Orlando", false, true, true);
        addServer("Julie", false, true, true);
        addServer("Bob", false, true, false);

        addServer("Daniel", false, false, false);
        addServer("Susan", false, false, false);
    }

    public void addServer(String name, boolean isAnon, boolean isOnShift, boolean isServing) {
        Server server = new Server();
        server.setId(UUID.randomUUID());
        server.setName(name);
        server.setAnon(isAnon);
        server.setServing(isServing);
        server.setOnShift(isOnShift);
        serverList.add(server);
    }

    public Server getServer(UUID serverId) {
        for (Server server : serverList) {
            if (server.getId().equals(serverId)) {
                return server;
            }
        }
        return null;
    }

    public List<Server> getServers() {
        return serverList;
    }
}
