package com.qudini.quicksetup.server.web;

public class AddServerRequest {

    private int onShiftAmount;
    private int servingAmount;

    public int getOnShiftAmount() {
        return onShiftAmount;
    }

    public void setOnShiftAmount(int onShiftAmount) {
        this.onShiftAmount = onShiftAmount;
    }

    public int getServingAmount() {
        return servingAmount;
    }

    public void setServingAmount(int servingAmount) {
        this.servingAmount = servingAmount;
    }
}
