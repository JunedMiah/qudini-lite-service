package com.qudini.quicksetup.server.web;

import com.qudini.quicksetup.server.service.Server;
import com.qudini.quicksetup.server.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "servers")
public class ServerController {

    private final ServerService serverService;

    @Autowired
    public ServerController(ServerService serverService) {
        this.serverService = serverService;
    }

    /**
     * @param addServerRequest.onShiftAmount - amount of staff we have available
     * @param addServerRequest.servingAmount - amount of people currently serving
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void addAnonymousServers(@RequestBody AddServerRequest addServerRequest) {
        int servingAmount = addServerRequest.getServingAmount();
        for (int i = 0; i < addServerRequest.getOnShiftAmount(); i++) {
            serverService.addServer("Server" + (i+1),  true, true, servingAmount != 0);
            if (servingAmount != 0) {
                servingAmount--;
            }
        }
    }

    @RequestMapping(method = RequestMethod.OPTIONS)
    public void options() {
        return;
    }

    @GetMapping
    public List<Server> getAllServers() {
        return serverService.getServers();
    }

    @GetMapping(value = "/{serverId}")
    public Server getServer(@PathVariable("serverId") UUID serverId) {
        return serverService.getServer(serverId);
    }


}
