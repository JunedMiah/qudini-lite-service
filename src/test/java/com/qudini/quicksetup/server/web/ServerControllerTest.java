package com.qudini.quicksetup.server.web;

import com.qudini.quicksetup.server.service.Server;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ServerControllerTest {

    @LocalServerPort
    private int randomServerPort;

    private String baseUrl;

    @Before
    public void setup() {
        baseUrl = "http://localhost:" + randomServerPort;
    }

    @Test
    public void shouldBeAbleToAddAndGetAllServers() {
        AddServerRequest addServerRequest = new AddServerRequest();
        addServerRequest.setOnShiftAmount(5);
        addServerRequest.setServingAmount(4);

        given().body(addServerRequest).post(baseUrl + "/servers");

        List<Server> servers = given().get(baseUrl + "/servers")
                .then()
                .extract()
                .body()
                .jsonPath().getList(".", Server.class);

        assertThat(servers.size()).isEqualTo(5);

        assertThat(servers.get(0).getId()).isNotNull();
        assertThat(servers.get(0).getName()).isNull();
        assertThat(servers.get(0).isAnon()).isTrue();
        assertThat(servers.get(0).isOnShift()).isTrue();
        assertThat(servers.get(0).isServing()).isTrue();

        assertThat(servers.get(1).getId()).isNotNull();
        assertThat(servers.get(1).getName()).isNull();
        assertThat(servers.get(1).isAnon()).isTrue();
        assertThat(servers.get(1).isOnShift()).isTrue();
        assertThat(servers.get(1).isServing()).isTrue();

        assertThat(servers.get(2).getId()).isNotNull();
        assertThat(servers.get(2).getName()).isNull();
        assertThat(servers.get(2).isAnon()).isTrue();
        assertThat(servers.get(2).isOnShift()).isTrue();
        assertThat(servers.get(2).isServing()).isTrue();

        assertThat(servers.get(3).getId()).isNotNull();
        assertThat(servers.get(3).getName()).isNull();
        assertThat(servers.get(3).isAnon()).isTrue();
        assertThat(servers.get(3).isOnShift()).isTrue();
        assertThat(servers.get(3).isServing()).isTrue();

        assertThat(servers.get(4).getId()).isNotNull();
        assertThat(servers.get(4).getName()).isNull();
        assertThat(servers.get(4).isAnon()).isTrue();
        assertThat(servers.get(4).isOnShift()).isTrue();
        assertThat(servers.get(4).isServing()).isFalse();
    }

    @Test
    public void shouldBeAbleToAddAndGetSingleServer() {
        AddServerRequest addServerRequest = new AddServerRequest();
        addServerRequest.setOnShiftAmount(5);
        addServerRequest.setServingAmount(4);

        given().body(addServerRequest).post(baseUrl + "/servers");

        List<Server> servers = given().get(baseUrl + "/servers")
                .then()
                .extract()
                .body()
                .jsonPath().getList(".", Server.class);

        UUID serverId = servers.get(0).getId();

        Server server = given()
                .get(baseUrl + "/servers/{serverId}", serverId)
                .then()
                .extract()
                .body()
                .as(Server.class);

        assertThat(server.getId()).isEqualTo(serverId);
        assertThat(server.getName()).isNull();
        assertThat(server.isAnon()).isTrue();
        assertThat(server.isOnShift()).isTrue();
        assertThat(server.isServing()).isTrue();
    }

}